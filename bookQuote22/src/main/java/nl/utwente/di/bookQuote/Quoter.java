package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Quoter {
    private Map<String, Double> myMapping = new HashMap<>();

    public Quoter() {
        myMapping.put("1", 10.00);
        myMapping.put("2", 45.00);
        myMapping.put("3", 20.00);
        myMapping.put("4", 50.00);
    }

    public double getBookPrice(String parameter) {
        if(myMapping.containsKey(parameter)) {
            return myMapping.get(parameter);
        }
        return 0.0;
    }

}
