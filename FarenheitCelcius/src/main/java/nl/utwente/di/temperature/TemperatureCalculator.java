package nl.utwente.di.temperature;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

public class TemperatureCalculator extends HttpServlet {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Calculator calculator;

    public void init() throws ServletException {
        calculator = new Calculator();
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {

        System.out.println(request.getRequestURL());
        System.out.println(request.getParameter("tranformTemp"));
        System.out.println(request.getParameter("myTemperature"));

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String tranformTemp = request.getParameter("tranformTemp");
        String transformedTemp = "";
        String displayText = "";
        if(tranformTemp.equals("f")){
            transformedTemp = "" + calculator.celciusToFahrenheit(Double.parseDouble(request.getParameter("myTemperature")));
            displayText = "" + "Fahrenheit";
        } else {
            displayText = "" + "Celcius";
            transformedTemp = "" + calculator.fahrenheitToCelcius(Double.parseDouble(request.getParameter("myTemperature")));
        }
        String docType =
                "<!DOCTYPE HTML>\n";
        String title = "Temperature Calculator";
        out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P> " +
                        transformedTemp + "\n" +
                        displayText +
                "</BODY></HTML>");
   }
}
