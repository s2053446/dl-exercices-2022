package nl.utwente.di.temperature;

public class Calculator {
    public double fahrenheitToCelcius(double fahrenheit){
        return (fahrenheit-32)*5/9;
    }

    public double celciusToFahrenheit(double celcius) {
        return (celcius*1.8)+32;
    }
}
