
// client book inputs for cart
var inputList = []; // clear later

function checkout() {
    var requestURL = "http://localhost:8080/onlineStore/rest/cart";
    let xmlhttp = new XMLHttpRequest();
       xmlhttp.onreadystatechange = function() {
            console.log("readyState code: ", this.readyState);
            console.log("status code: ", this.status);
           if (this.readyState === 4 && this.status === 204) {
                 //console.log(window.location);
                 location.href="checkout.html";
           }
       }
       xmlhttp.open("POST", requestURL, true); // restServlet
       xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
       // (element) =>
       // function (element) {}
       //var mappedElements = inputList.map((element) => ({item: element, numItems: 1})); // todo change
       var inputListText= JSON.stringify(inputList);
       xmlhttp.send(inputListText);
}

function checkDoubleNewItem(item) {
        for(var index = 0; index < inputList.length; index++) {
            if(inputList[index].itemID === item.itemID) {
                return inputList[index].count++; // exists
            }
        }
        return inputList.push({item: item, numItems: 1}) // not exists
}

function createCartItemList(event) {
    if (event.code === 'Enter') {
        let itemID = event.target.value;
        let xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                    if (this.readyState === 4 && this.status === 200 || this.status === 204) {
                        var myResponseItem = JSON.parse(this.responseText);
                        var parent = document.getElementById("book");
                        checkDoubleNewItem(myResponseItem);
                        console.log("THE INPUT LIST: ", inputList);
                        methodIterator(myResponseItem, parent);
                    }
                }
            var requestURL = "http://localhost:8080/onlineStore/rest/catalogue/" + itemID;
            xmlhttp.open("GET", requestURL, true); // restServlet
            xmlhttp.send();
    }
}


function calculateTotalCosts() {
    console.log("PAYMENT");
    var requestURL = "http://localhost:8080/onlineStore/rest/cart/costs";
    let theCosts = document.getElementById("costs");
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            var myResponseTotalCost = JSON.parse(this.responseText);
                theCosts.innerHTML = myResponseTotalCost;
        }
    }
    xmlhttp.open("GET", requestURL, true); // restServlet
    xmlhttp.send();
}

