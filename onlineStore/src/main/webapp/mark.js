function requestAllBooks(url) {
    let bookResult = document.getElementById("bookResult");
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            //bookResult.innerHTML = "These are the possible kids and tech books: \n" + this.responseText;
            var myResponseBooks = JSON.parse(this.responseText);
            handleResponse(myResponseBooks, "bookResult");
        }
    }
    xmlhttp.open("GET", url, true); // restServlet
    xmlhttp.send();
}

function loadBooks(event) {
    if (event.code === 'Enter') {
        let input = document.getElementById("search").value;
        if(input === "kids") {
            requestAllBooks("http://localhost:8080/onlineStore/rest/kids");
        } else if (input === "tech") {
            requestAllBooks("http://localhost:8080/onlineStore/rest/tech");
        }
    }
}

function handleResponse(jsonResponse, bookId) {
      console.log("the jsonResponse: ", jsonResponse);
      document.getElementById(bookId).innerHTML = '';
      var parent = document.getElementById(bookId);
      for(var index = 0; index < jsonResponse.length; index++) {
              var currentBook = jsonResponse[index];
              methodIterator(currentBook, parent);
      }
};

function methodIterator(currentBook, parent) {
  identifiers = ["itemID", "shortDescription", "longDescription", "cost"];
  for(var i = 0; i < identifiers.length; i++) {
      p = document.createElement("p");
      p.innerHTML = currentBook[identifiers[i]];
      parent.append(p);
      //parent.append(currentBook[identifiers[i]]);
  }
  seperater = document.createElement("p");
  seperater.innerHTML = "--------------------------------------------------------------------------";
  parent.append(seperater);
};

function handleResponseItemOrder(jsonResponse, bookId) {
      console.log("the jsonResponse: ", jsonResponse);
          document.getElementById(bookId).innerHTML = '';
          var parent = document.getElementById(bookId);
          for(var index = 0; index < jsonResponse.length; index++) {
                  var currentItem = jsonResponse[index];
                  methodIteratorItemOrder(currentItem, parent);
          }
}

function methodIteratorItemOrder(currentBook, parent) {
    identifiers = ["itemID", "shortDescription", "longDescription", "cost"];
      for(var i = 0; i < identifiers.length; i++) {
          p = document.createElement("p");
          console.log("currentBook: ", currentBook);
          console.log("currentBook[0]: ", currentBook[0]);
          theBook = currentBook.item;
          p.innerHTML = theBook[identifiers[i]];
          parent.append(p);
          //parent.append(currentBook[identifiers[i]]);
      }
      seperater = document.createElement("p");
      seperater.innerHTML = "--------------------------------------------------------------------------";
      parent.append(seperater);
}