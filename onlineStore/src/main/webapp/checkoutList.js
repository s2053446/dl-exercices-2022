function displayCart() {
        console.log("hello displayCart");
        var requestURL = "http://localhost:8080/onlineStore/rest/cart";
        let xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
             if (this.readyState === 4 && this.status === 200) {
                var myResponseItem = JSON.parse(this.responseText);
                console.log("!! response here: ", myResponseItem);
                handleResponseItemOrder(myResponseItem , "results");
             }
        }
        xmlhttp.open("GET", requestURL, true);
        xmlhttp.send();
        calculateTotalCosts();
}

function deleteShownCart() {
    var requestURL = "http://localhost:8080/onlineStore/rest/cart";
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
    if (this.readyState === 4 && this.status === 200) {
    var myResponseItem = JSON.parse(this.responseText);
        console.log("!! delete response: ", myResponseItem);
                    handleResponseItemOrder(myResponseItem , "results");
        }
    }
    xmlhttp.open("DELETE", requestURL, true);
    xmlhttp.send();
}

displayCart();
deleteShownCart();