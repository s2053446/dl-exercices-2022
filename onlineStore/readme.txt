This is a short description:

1) Open the webpage of the onlineStore
2.) Type kids or tech in the first search field.
    - type kids --> you get only the kids books
    - Remove kids from the search bar and type tech --> you get only the tech books descriptions now
3.) Type any itemId in the search field on the right (e.g. rowling001)
    When clicking enter the book description will be displayed below
    This book is added to the list of books which will define the shopping cart
4.) In this example remove rowling001 and type hall001 in the search bar on the right
    --> the description of the book is shown below the description of rowling001 now
5.) Click on checkout --> You are navigated to a new page which shows the final cart with the added books (the description of the books)
    as well as the final total price on this page